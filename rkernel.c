#ifndef _CODE16GCC_H_
#define _CODE16GCC_H_
__asm__(".code16gcc\n");
#endif
__asm__("jmp short main\n");

#define noinline  __attribute__((noinline))
#define regparm   __attribute__ ((regparm(3)))
#define noreturn  __attribute__((noreturn))


void noinline rmain();

void noinline regparm puts(const char* string);

void noreturn main(){
	rmain();
	while(1);
}

void noinline rmain(){
	char* start_msg = "Inicjalizowanie rkernel...";
	puts(start_msg);
	puts("\n\rWczytywanie GDT...");
}

void noinline regparm puts(const char* string) {
	while(*string) {
		__asm__ __volatile__ (
			"int 0x10"
			: /* no output */
			: "a" (0xE00 | *string), "b" (7)
		);
		string++;
	} 
}