dd if=/dev/zero of=test.img bs=512 count=1000
hexdump -C test.img
objdump -b binary -m i386 -D boot
ndisasm 

debug:
	qemu-system-i386 -s -S test.img

	gdb
	target remote localhost:1234
	info registers
	
