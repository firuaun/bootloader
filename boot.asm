[BITS 16]					; real mode (via BIOS) uses 16 bits
org 0x7C00 					; where program expects to be loaded (helps calculate some addresses)

global main

main:
	jmp short start
	nop

bootsector:
	OM db "RgOS    "
	SectSize dw 0x200
	NumPerSect db 0x1
	NumResvSect db 0x1
	DiskBoot db 0
	SectPerTrack dw 63
	HeadsCount dw 2
	ScnStagePA dw 0x201
	ScnStageStore dw 0x900


start:
	cli
	mov  [DiskBoot], dl		; id of disk that is booted from
	xor  ax, ax          	; zeroing ax (0x07c00)
	mov  ds, ax          	; DS = AX = 0x0
	mov  es, ax          	; ES = AX = 0x0
	mov  ss, ax          	; SS = AX = 0x0
	mov  sp, 0x7C00      	; Stack grows down from offset 0x7C00 toward 0x0000.
	mov si, welcome_msg
	call write_string
	mov si, loading_msg
	call write_string
	call reset_disks
	mov si, load_gdt_msg
	call write_string
	call load_gdt
	sti
	call read_disk
	mov si, rinit_jmp_msg
	call write_string
	push es
	push bx
	retf					; takes last two values from stack that is es:bx
	jmp $					; DE4D C0DE

	welcome_msg db "Witaj w RgOS!",0xA,0xD,0
	loading_msg db "Bootloader startuje...",0xA,0xD,0
	disk_reset_msg db "Resetowanie dysku...",0
	load_gdt_msg db "Wczytywanie GDT i IDT...",0
	disk_read_msg db "Odczytywanie kernela...",0
	rinit_jmp_msg db "Skok do rinit.",0xA,0xD,0
	fail_msg db "FAIL",0xA,0xD,0
	ok_msg db "OK",0xA,0xD,0

write_string:
	push ax
	push bx
	mov ah, 0xE				; prepare to call function to write in output an character

write_string_repeat:
	lodsb
	cmp al, 0 				; check if end of string
	je write_done			; if prev cmp == 0 then done
	mov bx, 0x9         	; Set bh (page nr) to 0, and bl (attribute) to white (9)
	int 0x10				; interrupt 0x10 to call output
	jmp write_string_repeat	; loop to the beginning
write_done:
	pop bx
	pop ax
	ret

;	AH - read subfunction
;	AL - segments to read
;	CH - cylinder
;	CL - sector
;	DH - head
;	DL - disk

read_disk:
	mov si, disk_read_msg
	call write_string
	mov ax, [ScnStageStore]	; temporary load address for buffer to ax
	mov es, ax 				; put ax to es (ES:BX for buffer)
	mov ax, [ScnStagePA]	; put physical address of second stage to AX
	call pa_to_lba			; physical address needs to be converted to lba
	push bx 				; save offset to jump after
	call lba_to_chs			; translate address and set all CSH registers for AH: 0x2 INT 0x13 
	mov dl, [DiskBoot]		; make sure dl is booted disk
	mov ah, 0x2 			; 0x2 - subfunction for read from disk
	mov al, 0x1 			; 0x1 - sectors to read
	xor bx,bx 				; es:bx -> [ScnStageStore]:0x1
	int 0x13
	pop bx
	jc print_fail			; jump if carry set, becouse of error
	call print_ok
	ret


;	IN: AX - LBAddress
;	OUT: CL - sector, DH - head, CH - cylinder
; 	formula:
;		sector = (lba mod sectorpertrack) + 1
;		cylinder = (lba / sectorpertrack) / HeadsCount
;		head = (lba / sectorpertrack) mod HeadsCount
lba_to_chs:
	xor dx,dx				; zeroes dx before division
	mov bx, [SectPerTrack]	; prepare number of sectors per track to be divisor
	div bx 					; divison in intel DX:AX / BX = AX (* BX + DX) <- 0:LBA / SectPerTrack = Result (* SectorPerTrack + Sector)
	inc dx 					; sector (dx) + 1
	push dx 				; push sector on stack

	xor dx, dx 				; need zero dx
	mov bx, [HeadsCount]	; in ax reminds the result of previous operation
	div bx 					; result of previous division / heads count = ax (rest: dx)

	mov ch, al 				; cylinders to ch
	xchg dh, dl 			; swap head to dh 

	pop ax 					; sector to ax
	mov cl, al 				; put sector to cl
	ret

;	IN: AX - physicall address
; 	OUT: AX - lba, BX - offset
pa_to_lba:
	push dx
	xor dx, dx
	mov bx, [SectSize]
	div bx

	mov bx, dx
	pop dx
	ret

reset_disks:
	mov si, disk_reset_msg
	call write_string
	mov dl, [DiskBoot]		; make sure dl is properly set to boot disk
	mov ax, 0 				; make sure ax is 0 - subfunction of bios that reset disk
	int 0x13				; bios interrupt 0x13
	jc print_fail			; jump if carry set, becouse of error
	call print_ok
	ret

print_fail:
	mov si, fail_msg
	call write_string
	ret

print_ok:
	mov si, ok_msg
	call write_string
	ret

idt:
	dw 0x800				; offset 2048 for 256 interrupts (8 bytes for each)
	dd 0 					; linear address of interrupts table

gdt:						; place for gdt
	dw 24					; bythes of gdt (2 bythes)
	dd 0x800				; address of gdt (4 bythes)

load_gdt:
	mov ax, 0
	mov es, ax
	mov di, 0x800			; set pointer to data at 0:0x800 where we store gdt
	; NULL segment description (needed)
	mov cx, 4
	rep stosw				; with cx = 4 (length) and ax = 0 (content) - repeats zero four times in [es:di]
	; CODE segment description
	mov [es:di], word 0xFFFF	; limit with grad on its actually 4GB (which is 20 bit but scattered)
	mov [es:di+2], word 0x0000	; base (which is 32 bit but scattered)
	mov [es:di+4], byte 0x0 	; still base
	mov [es:di+5], byte 0x9a	; access (made for read and execute by kernel)
	mov [es:di+6], byte 0xcf	; flags + limit
	mov [es:di+7], byte 0x0 	; base
	add di, 8
	; DATA segment description
	mov [es:di], word 0xFFFF	; limit with grad on its actually 4GB (which is 20 bit but scattered)
	mov [es:di+2], word 0x0000	; base (which is 32 bit but scattered)
	mov [es:di+4], byte 0x0 	; still base
	mov [es:di+5], byte 0x92	; access (made to write)
	mov [es:di+6], byte 0xcf	; flags + limit
	mov [es:di+7], byte 0x0 	; base
	lgdt [gdt]
	lidt [idt]
	call print_ok
	ret

	times 510-($-$$) db 0 	; pad to the 510 with 0
	dw 0xAA55 				; put magic number 	