disk: rkernel
	nasm -o boot boot.asm
	-rm -rf test.img
	# qemu feels weird about hard disks being <= 3MiB during bios read interrupt, unless it is floppy (-fda param)
	dd if=/dev/zero of=test.img bs=512 count=10000			
	./main.py
	./main.py test.img 513 rkernel

floppy:
	nasm -f bin -o bootit.bin boot.asm
	dd status=noxfer conv=notrunc if=bootit.bin of=bootit.flp

rkernel: rkernel.o
	ld -Tlinker.ld -static -nostdinc -nostdlib -nmagic -o rkernel.elf rkernel.o
	objcopy -O binary rkernel.elf rkernel

rkernel.o: rkernel.c
	gcc -c -g -I. -masm=intel -march=i386 -Os -ffreestanding -o rkernel.o rkernel.c -Wall

clean:
	rm -rf *.o *.elf *.img boot rkernel *.bin *.flp

run:
	qemu-system-i386 test.img

debug:
	qemu-system-i386 -s -S test.img